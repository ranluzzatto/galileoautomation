

USE [Galileo_SystemDB_QA]
GO

/*

Test Cases - 

1.irrigation program 1 - 2 cycles that where finished     (Cycle 1 started 4 hours ago and finished 3 hours ago, Cycle 2 started 2 hours age and ended 1 hour ago)

2.irrigation program 2 - 1 cycle that started but was not finished (Cycle that started 160 minuets ago and did not yet finish)

3.

	
*/

DECLARE @programName1 nvarchar(50) 
DECLARE @programName2 nvarchar(50) 

DECLARE @program1ExitStatus1 int = 1;
DECLARE @program1ExitStatus2 int = 0;

set @programName1 = '2 cycles that where finished'
set @programName2 = '1 cycle that was not finished'


/* Clear tables*/
DELETE FROM [Config.Log.OF].[FinishCycle]
      WHERE ControllerID=220215

DELETE FROM [Config.Log.OF].[StartCycle]
      WHERE ControllerID=220215



INSERT INTO [Config.Log.OF].[StartCycle]
           ([ControllerID]
           ,[Time]
           ,[SystemNumber]
           ,[ProgramNumber]
           ,[ProgramName]
           ,[TimeToEnd]
           ,[ReceivedDate])
     VALUES
           (220215 ,DATEADD(minute, -240, GETDATE())  ,3 ,1, @programName1 ,250 ,GETDATE()),
           (220215 ,DATEADD(minute, -120, GETDATE())  ,3 ,1, @programName1 ,250 ,GETDATE()),
           (220215 ,DATEADD(minute, -160, GETDATE())  ,3 ,2, @programName2 ,250 ,GETDATE())

INSERT INTO [Config.Log.OF].[FinishCycle]
           ([ControllerID]
           ,[Time]
           ,[SystemNumber]
           ,[ProgramNumber]
           ,[ProgramName]
           ,[FinishStatus]
           ,[ReceivedDate])
     VALUES		   

		   /* Irrigation finished at 'Time' and exited with status 'FinishStatus' */
           (220215 ,  DATEADD(minute, -180, GETDATE()),3 ,1,@programName1 ,@program1ExitStatus1 ,GETDATE()),	
           (220215 ,  DATEADD(minute, -60, GETDATE()),3 ,1,@programName1 ,@program1ExitStatus2 ,GETDATE()),	

		   /* Irrigation did not finished('Time=0') and still has StartCycle.TimeToEnd time for irrigation*/
           (220215 ,  0,3 ,2,@programName2 ,1 ,GETDATE())		   


/*********************************************************** program statuses  *****************************************************************/


DECLARE @i AS INT = 1
declare @programName nvarchar(50)

WHILE(@i <= 96)/*There are 13 possible statuses*/
BEGIN


set @programName = 'program ' + cast(@i as varchar(50)) + '(status' + cast(@i%14 as varchar(50))+ ')'

	INSERT INTO [Config.Log.OF].[StartCycle]
           ([ControllerID]
           ,[Time]
           ,[SystemNumber]
           ,[ProgramNumber]
           ,[ProgramName]
           ,[TimeToEnd]
           ,[ReceivedDate])
     VALUES
           (220215 ,DATEADD(minute, -(140 + @i*5), GETDATE())  ,3 ,@i + 2, @programName ,250 ,GETDATE())

INSERT INTO [Config.Log.OF].[FinishCycle]
           ([ControllerID]
           ,[Time]
           ,[SystemNumber]
           ,[ProgramNumber]
           ,[ProgramName]
           ,[FinishStatus]
           ,[ReceivedDate])
     VALUES		   

		   /* Irrigation finished at 'Time' and exited with status 'FinishStatus' */
           (220215 ,  DATEADD(minute, -(40 + @i*5), GETDATE()),3 ,@i + 2,@programName ,@i%14 ,GETDATE())

    SET @i += 1
END


SELECT [ID]
      ,[ControllerID]
      ,[Time]
      ,[SystemNumber]
      ,[ProgramNumber]
      ,[ProgramName]
      ,[TimeToEnd]
      ,[ReceivedDate]
  FROM [Galileo_SystemDB_QA].[Config.Log.OF].[StartCycle]


SELECT  [ID]
      ,[ControllerID]
      ,[Time]
      ,[SystemNumber]
      ,[ProgramNumber]
      ,[ProgramName]
      ,[FinishStatus]
      ,[ReceivedDate]
  FROM [Galileo_SystemDB_QA].[Config.Log.OF].[FinishCycle]

  
GO
